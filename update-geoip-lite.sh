#! /bin/sh
###############################################################################
# Created by Steve Scotter @ Spectrum Computer Solutions
# 07-10-2014
#
# This script will fetch a zipped GeoIP database files from the maxmind.com website, pipe that into gzip which spits out the uncompressed file in the correct location.
# Doing it this way prevents having to cleanup temp files afterwards
#
# ln -s /sps/freebsd/update-geoip-lite.sh /etc/periodic/weekly/600.update-geoip-lite.sh
#
###############################################################################

# Note how the output filenames of GeoLiteCity and GeoLiteCityv6 are changed to GeoIPCity and GeoIPCityv6.
# This is because we're using the free edition but the various GeoIP libraries still expect the licensed version filenames

logger -s -t update-geoip-lite.sh -p cron.info "starting...."

DEST_DIR="/usr/local/share/GeoIP"
# TEMP_DIR="$(mktemp -d '/usr/local/share/GeoIP/GeoIPupdate.XXXXXX')"
TEMP_DIR="$(mktemp -d '/tmp/GeoIPupdate.XXXXXX')"
EXTRA_PARAMS="--timeout=5 --verbose --verbose --verbose --ipv4-only"
#EXTRA_PARAMS="--timeout=5 --ipv4-only"
#EXTRA_PARAMS=""

logger -s -t update-geoip-lite.sh -p cron.info "TEMP_DIR = $TEMP_DIR"

fetch $EXTRA_PARAMS -o - http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz | gzip -d > $TEMP_DIR/GeoIP.dat
GeoIP_result=$?
logger -s -t update-geoip-lite.sh -p cron.info "GeoIP_result=$GeoIP_result"
if [ $GeoIP_result -eq 0 ] ; then
	logger -s -t update-geoip-lite.sh -p cron.info "Moving $TEMP_DIR/GeoIP.dat to $DEST_DIR/GeoIP.dat"
	mv $TEMP_DIR/GeoIP.dat $DEST_DIR/GeoIP.dat
fi

fetch $EXTRA_PARAMS -o - http://geolite.maxmind.com/download/geoip/database/GeoIPv6.dat.gz | gzip -d > $TEMP_DIR/GeoIPv6.dat
GeoIPv6_result=$?
if [ $GeoIPv6_result -eq 0 ] ; then
   logger -s -t update-geoip-lite.sh -p cron.info "Moving $TEMP_DIR/GeoIPv6.dat to $DEST_DIR/GeoIPv6.dat"
   mv $TEMP_DIR/GeoIPv6.dat $DEST_DIR/GeoIPv6.dat
fi

fetch $EXTRA_PARAMS -o - http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz | gzip -d > $TEMP_DIR/GeoIPCity.dat
GeoIPCity_result=$?
if [ $GeoIPCity_result -eq 0 ] ; then
   logger -s -t update-geoip-lite.sh -p cron.info "Moving $TEMP_DIR/GeoIPCity.dat to $DEST_DIR/GeoIPCity.dat"
   mv $TEMP_DIR/GeoIPCity.dat $DEST_DIR/GeoIPCity.dat
fi

fetch $EXTRA_PARAMS -o - http://geolite.maxmind.com/download/geoip/database/GeoLiteCityv6-beta/GeoLiteCityv6.dat.gz | gzip -d > $TEMP_DIR/GeoIPCityv6.dat
GeoIPCityv6_result=$?
if [ $GeoIPCityv6_result -eq 0 ] ; then
   logger -s -t update-geoip-lite.sh -p cron.info "Moving $TEMP_DIR/GeoIPCityv6.dat to $DEST_DIR/GeoIPCityv6.dat"
   mv $TEMP_DIR/GeoIPCityv6.dat $DEST_DIR/GeoIPCityv6.dat
fi

fetch $EXTRA_PARAMS -o - http://download.maxmind.com/download/geoip/database/asnum/GeoIPASNum.dat.gz | gzip -d > $TEMP_DIR/GeoIPASNum.dat
GeoIPASNum_result=$?
if [ $GeoIPASNum_result -eq 0 ] ; then
   logger -s -t update-geoip-lite.sh -p cron.info "Moving $TEMP_DIR/GeoIPASNum.dat to $DEST_DIR/GeoIPASNum.dat"
   mv $TEMP_DIR/GeoIPASNum.dat $DEST_DIR/GeoIPASNum.dat
fi

fetch $EXTRA_PARAMS -o - http://download.maxmind.com/download/geoip/database/asnum/GeoIPASNumv6.dat.gz | gzip -d > $TEMP_DIR/GeoIPASNumv6.dat
GeoIPASNumv6_result=$?
if [ $GeoIPASNumv6_result -eq 0 ] ; then
   logger -s -t update-geoip-lite.sh -p cron.info "Moving $TEMP_DIR/GeoIPASNumv6.dat to $DEST_DIR/GeoIPASNumv6.dat"
   mv $TEMP_DIR/GeoIPASNumv6.dat $DEST_DIR/GeoIPASNumv6.dat
fi

rmdir "$TEMP_DIR"

echo "update-geoip-lite.sh end"
